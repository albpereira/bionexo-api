FROM java:8-jdk-alpine
COPY target/api-bionexo.war /home/api-bionexo.war
ADD target/api-bionexo.war /
EXPOSE 8080
CMD ["java", "-Dserver.port=8080","-Dparameter.distance.meters=2500","-jar", "/home/api-bionexo.war"]
